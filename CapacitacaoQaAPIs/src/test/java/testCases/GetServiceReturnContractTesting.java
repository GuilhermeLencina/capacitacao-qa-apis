package testCases;

import static io.restassured.RestAssured.given;

import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import testBases.ServiceReturn;

public class GetServiceReturnContractTesting extends ServiceReturn {
	@Test
    public void ServiceReturnTest(){
        Response payload =
        given()
                .baseUri("https://viacep.com.br")
                .basePath("/ws/RS/Gravatai/Barroso/json/")
        .when()
                .get()
        .then()
                .log().all()
                .statusCode(200).extract().response();

        ArrayList<String> cep = payload.then().extract().path("cep");

        String[] expectedCep = {"94085-170", "94175-000"};

        
        Assertions.assertArrayEquals(expectedCep, cep.toArray());
        
        payload.then().body(JsonSchemaValidator.matchesJsonSchemaInClasspath("Schemas/GetServiceReturnJsonSchema.json"));
    }
}
