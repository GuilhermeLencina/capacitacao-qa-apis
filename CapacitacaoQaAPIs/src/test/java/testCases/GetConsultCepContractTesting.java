package testCases;

import static io.restassured.RestAssured.given;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import testBases.ConsultCep;

public class GetConsultCepContractTesting extends ConsultCep{
	@Test
    public void ContractTestXML(){
        Response payload =
        given()
                .baseUri("https://viacep.com.br")
                .basePath("/ws/91060900/json/")
        .when()
                .get()
        .then()
                .log().all()
                .statusCode(200).extract().response();

        String logradouro = payload.then().extract().path("logradouro");
        String deveria = "Avenida Assis Brasil 3940";

        Assertions.assertEquals(deveria, logradouro);
        
        payload.then().body(JsonSchemaValidator.matchesJsonSchemaInClasspath("Schemas/GetConsultCepJsonSchema.json"));
    }
}
