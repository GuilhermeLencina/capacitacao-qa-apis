package testCases;

import static io.restassured.RestAssured.given;

import org.junit.jupiter.api.Test;

import testBases.ConsultCepNonexistent;


public class GetConsultCepNonexistent extends ConsultCepNonexistent {
	@Test
    public void ConsultCepNonexistent(){
        given()
        	.spec(requestSpec)
        .when()
                .get()
        .then()
        	.log().all()
        	.spec(responseSpec)
        	
        ;
    }
}
