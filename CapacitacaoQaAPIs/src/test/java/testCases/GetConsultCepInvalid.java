package testCases;

import static io.restassured.RestAssured.given;

import org.junit.jupiter.api.Test;

import testBases.ConsultCepInvalid;

public class GetConsultCepInvalid extends ConsultCepInvalid{
	@Test
    public void ConsultCepInvalid(){
        given()
        		.spec(requestSpec)
        .when()
                .get()
        .then()
        		.log().all()
        		.spec(responseSpec);
        
        System.out.println("CEP INV�LIDO");
        ;
    }
}
