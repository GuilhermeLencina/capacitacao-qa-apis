package testCases;

import static io.restassured.RestAssured.given;

import org.junit.jupiter.api.Test;

import testBases.ConsultCep;

public class GetConsultCep extends ConsultCep {
	@Test
    public void ConsultCep(){
        given()
                .spec(requestSpec)
        .when()
                .get()
        .then()
                .log().all()
                .spec(responseSpec)
        ;
    }
}
