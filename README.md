# Projeto: Mentoria de testes automatizados APIs com JUnit5

Este projeto tem por objetivo testar minhas habilidades em TESTES e AUTOMAÇÕES com APIs no qual tive que simular uma compra com sucesso no site "https://viacep.com.br/ws/91060900/json/"

**Tecnologias Utilizadas**

Este projeto conta com as seguintes tecnologias:  

- Java (Ver. 1.8.0)
- JUnit5 (Ver. 5.7.1)
- Gradle  (Ver. 7.0-milestone-3)
- Rest-assured  (Ver. 4.3.0)


**Padrões Utilizados**

**Main/Java**

- **testBases** : Pasta onde se encontram classes de abstracao.

**Main/Resources**

Pasta onde se encontram arquivos de inputs para execução dos testes.

**Test/Java**

- **testCases** : Pasta onde se encontram classes e métodos de Testes.

**Allure**

- **Como rodar:** : Ir na pasta raiz do projeto, abrir o console e digitar "Allure serve build/test-results"





